// JavaScript Document
$(document).ready(function(){
	//Home slider
   
    $('.slider-part').owlCarousel({
		  navigation : false,
		  pagination : false,
		  autoPlay : 6000,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem:true
	});
   
	
	//jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

	//Menu close
	menu_close();
	

});
/**************************Document.ready end**************************/


$(window).scroll(function() {
	//jQuery to collapse the navbar on scroll
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
	} else {
			$(".navbar-fixed-top").removeClass("top-nav-collapse");
	}
	
	
});

$(window).resize(function(){
	//Menu close
	menu_close();
	
	
});


function menu_close(){
	if($(window).width()<=741){
		$('.main-navbar .nav li').click(function(){
			$(this).parents('.main-navbar').find('.navbar-toggle').trigger('click');
		});
	}
}










